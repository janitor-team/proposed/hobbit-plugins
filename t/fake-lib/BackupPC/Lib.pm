# -*- perl -*-

# A minimalistic BackupPC::Lib mockup as we can't expect people to
# install a full BackupPC instance. -- Axel
#
# Copyright (C) 2015 Axel Beckert <abe@debian.org>
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
#  0. You just DO WHAT THE FUCK YOU WANT TO.

package BackupPC::Lib;

sub new {
    my %bla;
    bless(\%bla, shift);
    return \%bla;
}

foreach my $f (qw(ServerConnect ChildInit Conf ServerMesg)) {
    eval "sub $f { return undef; }";
}

return 1;
